import asyncio

from async_data_handler import AsyncDataHandler
from async_extractor import AsyncExtractor

if __name__ == "__main__":
    try:
        loop = asyncio.get_event_loop()

        uris = [
            'http://headers.jsontest.com/',
            'http://ip.jsontest.com/'
        ]

        extractor = AsyncExtractor(loop, uris=uris)
        handler = AsyncDataHandler(extractor)
        loop.run_until_complete(handler.handle())
    except Exception as e:
        print('ERROR: {}'.format(str(e)))
    finally:
        loop.close()
