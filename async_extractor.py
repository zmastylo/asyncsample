import asyncio
import concurrent

import requests
from aiohttp import ClientSession


class AsyncExtractor:
    __RATE_LIMIT_REACHED = 429

    def __init__(self, event_loop, uris, max_workers=10):
        self.loop = event_loop
        self.max_workers = max_workers
        self.uris = uris

    async def extract(self):
        return await self.__gather()

    async def __gather(self):
        """
        Go over list of uris (urls) and process get requests
        asynchronously. That of course, can be done for just
        one url
        :return: futures
        """
        try:
            futures = [
                self.loop.run_in_executor(None, self.__get, uri)
                for uri in self.uris
            ]
            return futures
        except Exception as e:
            print('AsyncExtractor.__gather - error: shit blew up' + str(e))
            raise e

    def __get(self, url):
        """
        A regular GET, wrapped so you can account for various
        statuses if you want to etc
        :param url:
        :return:
        """
        response = requests.get(url=url)
        if response.status_code == self.__RATE_LIMIT_REACHED:
            raise Exception('Rate limit exceeded - need to wait 1 hour')
        return response

    # The code in below shows other ways of doing what we do above
    # I like the above
    async def __gather_tpe(self):
        """
        You can employ a specific executor as in here we use
        concurrent.futures.ThreadPoolExecutor. No need really, what you see
        in __gather() is good.
        :return:
        """
        try:
            with concurrent.futures.ThreadPoolExecutor(max_workers=self.max_workers) as tpe:
                futures = [
                    self.loop.run_in_executor(tpe, requests.get, uri)
                    for uri in self.uris
                ]
            return futures
        except Exception as e:
            print('AsyncExtractor.__gather - error: shit blew up' + str(e))
            persist_last_id(self.id_list[0])
            raise e

    async def __gather_with_session(self):
        """
        Another way of sending a get with aiohttp session
        I didn't see much difference in performance doing this
        vs normal wrapped get (as in __get)
        :return:
        """
        async with ClientSession() as session:
            tasks = []
            for uri in self.uris:
                tasks.append(self.__get(session, url=uri))

            result = await asyncio.gather(*tasks, return_exceptions=True)
            return result

    async def __get_with_session(self, session: ClientSession, url, **kwargs):
        """
        Another way to do async GET via ClientSession. I didn't see much difference
        vs requests.get plugged into execution loop:
        elf.loop.run_in_executor(tpe, requests.get, uri
        :param session: client session
        :param url:
        :param kwargs:
        :return: data retrieved via get
        """
        response = await session.request('GET', url=url, **kwargs)

        # Example status showing rate limit reach on a certain API
        # not like we will have it, but for illustration purposes
        if response.status == self.__RATE_LIMIT_REACHED:
            raise Exception('Rate limit exceeded - need to wait 1 hour')

        # Assuming you expect JSON
        data = await response.json()
        return data
